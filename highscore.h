//////////////////////////////////////////////////////////////////////////////////////////////////////
//  Namn:   Jimmy M�kkeli & Carl Larsson                                                            //
//  ID:     jmi14002 & cln14008                                                                     //
//                                                                                                  //
//  Beskrivning                                                                                     //
//  Den h�r filen har hand om all hantering av High Score listan, hur programmet l�ser av textfilen //
//	och placerar in alla spelare och deras po�ng i en l�nkad lista, samt hantering av att l�gga till//
//	nya High Scores.Det finns �ven funktioner f�r hur listan ska sparas och hur minnet frig�rs n�r  //
//	allt �r f�rdigt.																				//
//////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef HIGHSCORE_H
#define HIGHSCORE_H
#include "structs.h"

int highscore_menu(ALLEGRO_EVENT_QUEUE **event_queue, struct Loaded_Images **LoadedImages, ALLEGRO_EVENT *ev, ALLEGRO_SAMPLE_INSTANCE **menuSongInstance, ALLEGRO_FONT **fontPtr, int tempScore);
int openFile(FILE **textFilePtr, char wordPtr[13]);
int fillList(FILE **textFilePtr, struct HighScoreList **currentPtr);
void outPutList(struct HighScoreList **currentPtr, struct Loaded_Images **LoadedImages, ALLEGRO_FONT **fontPtr, int inputAllowed, char name[11]);
void FreeMemory(struct HighScoreList **currentPtr);
int searchList(struct HighScoreList **currentPtr, struct HighScoreList **previous, char testScore[11]);
void placeNewScore(struct HighScoreList **currentPtr, struct HighScoreList **previous, char testScore[11], char testName[11]);
struct HighScoreList *placeNewHighestScore(struct HighScoreList **currentPtr, char testScore[11], char testName[11]);
void saveListToFile(FILE **textFilePtr, char wordPtr[13], struct HighScoreList **currentPtr);

#endif