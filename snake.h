//////////////////////////////////////////////////////////////////////////////////////////////////////
//  Namn:   Jimmy M�kkeli & Carl Larsson                                                            //
//  ID:     jmi14002 & cln14008                                                                     //
//                                                                                                  //
//  Beskrivning                                                                                     //
//  Den h�r filen har hand om alla funktioner som �r specifika till sj�lva spelet. Hantering av     //
//  kollision, f�rflyttning, hur ormen v�xer.														//
//  Den hanterar �ven uppdatering av spelarens po�ng och frig�ranet av minne i den l�nkade listan   //
//  och hur nya �pplen och power up skapas															//
//  p� spelplanen.																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef SNAKE_H
#define SNAKE_H
#include "structs.h"

// Function prototypes
int add_node(struct SnakeBody **travel, int xPos, int yPos);											// Add a new node to the list
void update_snake(struct SnakeBody **Head, int x, int y);												// Shift the values of X & Y in the list
void delete_list(struct SnakeBody **travel);															// Clear memory, delete each entry
int snake_grow(struct SnakeHead *HeadPtr, struct SnakeBody *travelPtr, int *addNodePtr);				// Handles add_node and speeds up the game
void check_collision(struct SnakeHead *HeadPtr, struct Apple *applePtr, int *addNodePtr,
					 struct Face *FacePtr, struct PowerUp *powerUpPtr, struct PlayerSound **pSounds);	// Collision detection
void update_score(char scoreString[11], char currentScoreString[11], int currScore);					// Handles the conversion from int to string, score stuff
int move_snake(struct SnakeHead *HeadPtr);
void spawn_powerup(struct PowerUp *powerUpPtr, struct SnakeHead *headPtr, struct Apple *applePtr);		// Handles powerup spawning
void spawn_apple(struct Apple *applePtr, struct SnakeHead *headPtr);									// Handles apple spawning
void change_face(struct Face *Face, struct SnakeHead *HeadPtr, struct PowerUp *powerUpPtr);				// Changes the current face, depending on speed or death

#endif
