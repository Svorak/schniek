﻿//////////////////////////////////////////////////////////////////////////////////////////////////////
//  Namn:   Jimmy Mäkkeli & Carl Larsson                                                            //
//  ID:     jmi14002 & cln14008                                                                     //
//                                                                                                  //
//  Beskrivning                                                                                     //
//  Det här är huvudfilen som har hand om att binda samman alla funktioner i programmet. Det finns  //
//  funktioner för hantering av input samt hur huvudmenyn fungerar och renderas. Det finns även en  //
//  funktion för logiken i själva spelet samt hur spelet renderas.                                  //
//////////////////////////////////////////////////////////////////////////////////////////////////////
#include "structs.h"
#include "init.h"
#include "snake.h"
#include "highscore.h"

/*
									,,ggddY"""Ybbgg,,
								 ,agd888b,_ "Y8, ___`""Ybga,
							  ,gdP""88888888baa,.""8b    "888g,
							,dP"     ]888888888P'  "Y     `888Yb,
						  ,dP"      ,88888888P"  db,       "8P""Yb,
						 ,8"       ,888888888b, d8888a           "8,
						,8'        d88888888888,88P"' a,          `8,
					   ,8'         88888888888888PP"  ""           `8,
					   d'          I88888888888P"                   `b
					   8           `8"88P""Y8P'                      8
					   8            Y 8[  _ "                        8
					   8              "Y8d8b  "Y a                   8
					   8                 `""8d,   __                 8
					   Y,                    `"8bd888b,             ,P
					   `8,                     ,d8888888baaa       ,8'
						`8,                    888888888888'      ,8'
						 `8a                   "8888888888I      a8'
						  `Yba                  `Y8888888P'    adP'
						   "Yba                 `888888P'   adY"
						     `"Yba,             d8888P" ,adP"'
								`"Y8baa,      ,d888P,ad8P"'
									 ``""YYba8888P""''

???  ???  ??????   ??????? ???  ???   ????????? ???  ??? ????????    ???????  ???       ??????  ????   ???? ??????? ?????????
???  ??? ???????? ???????? ??? ????   ????????? ???  ??? ????????    ???????? ???      ???????? ?????  ???? ??????? ?????????
???????? ???  ??? ???      ???????       ???    ???????? ??????      ???????? ???      ???????? ?????? ???? ?????      ???
???????? ???????? ???      ???????       ???    ???????? ??????      ???????  ???      ???????? ??????????? ?????      ???
???  ??? ???  ??? ???????? ???  ???      ???    ???  ??? ????????    ???      ???????? ???  ??? ??? ??????? ???????    ???
???  ??? ???  ???  ??????? ???  ???      ???    ???  ??? ????????    ???      ???????? ???  ??? ???  ?????? ???????    ???

*/

void check_input(ALLEGRO_EVENT *evPtr, int *keyLockPtr, struct SnakeHead *HeadPtr, int *addNodePtr, int *runningPtr);
void draw_screen(struct Apple *applePtr, struct Loaded_Images **LoadedImages, struct SnakeHead *headPtr, struct SnakeBody *travelPtr, struct Face *Face, ALLEGRO_FONT **fontPtr, char scoreString[11],
				 struct PowerUp *powerUpPtr);
void draw_menu(struct Loaded_Images **LoadedImages);
int main_game(ALLEGRO_EVENT_QUEUE **event_queue, struct Loaded_Images **LoadedImages, ALLEGRO_EVENT *ev, ALLEGRO_FONT **fontPtr, ALLEGRO_SAMPLE_INSTANCE **songInstance
			 , struct PlayerSound **pSounds);
int main_menu(ALLEGRO_EVENT_QUEUE **event_queue, struct Loaded_Images **LoadedImages, ALLEGRO_EVENT *ev, ALLEGRO_SAMPLE_INSTANCE **menuSongInstance);

int main()
{
	int appRunning = 1;
	int tempt = 0;
	int tempScoreCheck = 0;
	int highScoreCheck = 0;

	// Allegro stuff
	ALLEGRO_DISPLAY *display = NULL;				// Handles display, screen size and buffers
	ALLEGRO_EVENT_QUEUE *event_queue = NULL;		// Queue events
	ALLEGRO_TIMER *timer = NULL;					// Timer ._.
	ALLEGRO_EVENT ev;								// Check for events, like keyboard inputs or timer event
	ALLEGRO_FONT *font = NULL;

	// Bitmaps
	struct Loaded_Images *LoadedImages = malloc(sizeof(struct Loaded_Images));
	if (LoadedImages == NULL) {
		al_show_native_message_box(display, "Error", "Error", "Failed to allocate images!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return -1;
	}

	// Audio
	ALLEGRO_SAMPLE *sample = NULL;					// Used to load sample from disk
	ALLEGRO_SAMPLE_INSTANCE *songInstance = NULL;	// Used to be able to play long samples, like music
	ALLEGRO_SAMPLE *menuSample = NULL;
	ALLEGRO_SAMPLE_INSTANCE *menuSongInstance = NULL;
	struct PlayerSound *pSounds = malloc(sizeof(struct PlayerSound));
	if (pSounds == NULL) {
		al_show_native_message_box(display, "Error", "Error", "Failed to allocate sounds!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return -1;
	}

	// Seed random
	srand(time(0));

	// Initialize stuff
	if (init_stuff(&display, &event_queue, &timer) == -1) {
		appRunning = 0;
	}
	if (load_images(&LoadedImages, &display) == -1) {
		al_show_native_message_box(display, "Error", "Error", "Failed to load images!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
		appRunning = 0;
	}

	if (load_sample(&sample, &songInstance, &menuSample, &menuSongInstance, &pSounds) == -1) {
		al_show_native_message_box(display, "Error", "Error", "Failed to load sounds!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
		appRunning = 0;
	}
	if (load_font(&font) == -1) {
		al_show_native_message_box(display, "Error", "Error", "Failed to load fonts!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
		appRunning = 0;
	}

	// Start the timer, for game timing stuff
	al_start_timer(timer);

	// Main Program Loop
	while (appRunning == 1) {
		// Main menu
		tempt = 0;
		tempt = main_menu(&event_queue, &LoadedImages, &ev, &menuSongInstance);

		// MAIN GAME //

		if (tempt == 1) {
			tempScoreCheck = main_game(&event_queue, &LoadedImages, &ev, &font, &songInstance, &pSounds);
			if (tempScoreCheck == -1) {
				al_show_native_message_box(display, "Error", "Error", "Failed to allocate memory!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
				return -1;
			}

			else if (tempScoreCheck >= 0) {
				highScoreCheck = highscore_menu(&event_queue, &LoadedImages, &ev, &menuSongInstance, &font, tempScoreCheck);

				if (highScoreCheck == -1) {
				al_show_native_message_box(display, "Error", "Error", "Failed to allocate memory!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
				return -1;
                }

                if (highScoreCheck == -2) {
				al_show_native_message_box(display, "Error", "Error", "Failed to open file!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
				return -1;
                }
				al_play_sample_instance(menuSongInstance);
            }
		}
		if (tempt == 2)
		{
			highScoreCheck = highscore_menu(&event_queue, &LoadedImages, &ev, &menuSongInstance, &font, tempScoreCheck);
			if (highScoreCheck == -1) {
				al_show_native_message_box(display, "Error", "Error", "Failed to allocate memory!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
				return -1;
			}
			if (highScoreCheck == -2) {
				al_show_native_message_box(display, "Error", "Error", "Failed to open file!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
				return -1;
			}

		}
		if (tempt == 0) {
			appRunning = 0;
		}
	}

	//////////////
	// Clean up //
	//////////////
	al_destroy_display(display);
	al_destroy_bitmap(LoadedImages->snakePart);
	al_destroy_bitmap(LoadedImages->apple);
	al_destroy_bitmap(LoadedImages->background);
	al_destroy_timer(timer);
	al_destroy_sample(sample);
	al_destroy_sample_instance(songInstance);
	al_destroy_sample(menuSample);
	al_destroy_sample_instance(menuSongInstance);
	al_destroy_sample(pSounds->applePickup);
	al_destroy_sample(pSounds->blindnessPickup);
	al_destroy_sample(pSounds->bonusApplePickup);
	al_destroy_sample(pSounds->death);
	al_destroy_sample(pSounds->powerUpPickup);

	free(LoadedImages);

	return 0;
}

///////////////////
// Handles input //
///////////////////
void check_input(ALLEGRO_EVENT *evPtr, int *keyLockPtr, struct SnakeHead *HeadPtr, int *addNodePtr, int *runningPtr)
{
	// Start by checking if the keyinput is locked, to prevent fast tapping to change direction, making it possible to crash into yourself
	// Then check if the controls are inverted or not
	switch (evPtr->keyboard.keycode) {
		case ALLEGRO_KEY_UP:
			if (HeadPtr->direction != DOWN && *keyLockPtr == 0) {
				// Check if the controls are inverted
				if (HeadPtr->invertedControlsPower == 1 && HeadPtr->direction != UP) {
					HeadPtr->direction = DOWN;
				}
				else {
					HeadPtr->direction = UP;
				}
				*keyLockPtr = 1;
			}

			break;

		case ALLEGRO_KEY_DOWN:
			if (HeadPtr->direction != UP && *keyLockPtr == 0) {
				// Check if the controls are inverted
				if (HeadPtr->invertedControlsPower == 1 && HeadPtr->direction != DOWN) {
					HeadPtr->direction = UP;
				}
				else {
					HeadPtr->direction = DOWN;
				}

				*keyLockPtr = 1;
			}

			break;

		case ALLEGRO_KEY_LEFT:
			if (HeadPtr->direction != RIGHT && *keyLockPtr == 0) {
				// Check if the controls are inverted
				if (HeadPtr->invertedControlsPower == 1 && HeadPtr->direction != LEFT) {
					HeadPtr->direction = RIGHT;
				}
				else {
					HeadPtr->direction = LEFT;
				}

				*keyLockPtr = 1;
			}

			break;

		case ALLEGRO_KEY_RIGHT:
			if (HeadPtr->direction != LEFT && *keyLockPtr == 0) {
				// Check if the controls are inverted
				if (HeadPtr->invertedControlsPower == 1 && HeadPtr->direction != RIGHT) {
					HeadPtr->direction = LEFT;
				}
				else {
					HeadPtr->direction = RIGHT;
				}

				*keyLockPtr = 1;
			}

			break;

		// For debugging only
		case ALLEGRO_KEY_SPACE:
			*addNodePtr = 1;
			*keyLockPtr = 1;
			break;

		// Exit game
		case ALLEGRO_KEY_Q:
			*runningPtr = 0;
			break;
	}
}

/////////////////////////////////////////////////////////////
// Function to draw the screen while in the main game loop //
/////////////////////////////////////////////////////////////
void draw_screen(struct Apple *applePtr, struct Loaded_Images **LoadedImages, struct SnakeHead *headPtr, struct SnakeBody *travelPtr, struct Face *Face, ALLEGRO_FONT **fontPtr, char scoreString[11],
struct PowerUp *powerUpPtr)
{
	// Clear screen
	al_clear_to_color(al_map_rgb(0, 0, 0));

	// Draw background
	al_draw_bitmap((*LoadedImages)->background, 0, 0, 0);

	// Draw apple
	if (applePtr->appleType == 1) {
		al_draw_bitmap((*LoadedImages)->apple, applePtr->xPos, applePtr->yPos, 0);
	}
	// Draw glowing apple
	else if (applePtr->appleType > 1) {
		al_draw_bitmap((*LoadedImages)->appleBonusPoint, applePtr->xPos, applePtr->yPos, 0);
	}

	// Draw powerups
	if (powerUpPtr->isSpawned == 1) {
		al_draw_bitmap((*LoadedImages)->PowerUps[powerUpPtr->powerType], powerUpPtr->xPos, powerUpPtr->yPos, 0);
	}


	// Draw snake, head first, only if the snake is alive, check if ghost mode is on
	if (headPtr->isDead == 0 && headPtr->blindModePower == 0) {
		al_draw_bitmap((*LoadedImages)->snakePart, headPtr->xPos, headPtr->yPos, 0);
		travelPtr = headPtr->nextPart;

		// Scan the list for nodes to render
		while (travelPtr != NULL) {
			al_draw_bitmap((*LoadedImages)->snakePart, travelPtr->xPos, travelPtr->yPos, 0);
			travelPtr = travelPtr->nextPart;
		}
	}

	// Draw Current Face if snake is not dead
	if (Face->currentFace > 0 && Face->displayTimer > 0 && headPtr->isDead == 0) {
		al_draw_bitmap((*LoadedImages)->Faces[Face->currentFace], Face->xPos, Face->yPos, 0);
	}
	// Draw CurrentDefault Face if no other face should be drawn
	else
		al_draw_bitmap((*LoadedImages)->Faces[Face->currentDefaultFace], Face->xPos, Face->yPos, 0);

	// Draw text
	al_draw_text(*fontPtr, al_map_rgb(255, 255, 255), 727, 276, ALLEGRO_ALIGN_LEFT, scoreString);

	// Render screen
	al_flip_display();
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　）													//
//　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　ヽ　MAIN GAME LOOP！！								//
//　　　　　　　　　　　　　　　　　　　 ,ﾍ　　　　　　　 　 　.へ　　　）												//
//　　　　　　　　　　　　　　　　　,.　'　　〉=γ''≫'´V`ヽ､'　　 ＼　`ｖ'⌒ヽ／⌒ヽ／__ ,. ‐-　.. _						//
//　　　　　　　　　　　 　 　 .　'´ 　 　 //〃／/ , ､　　　＼､. 　　＼　　　　　　 _,ィ７／　　__　　｀` ｰ- 、			//
//　　　　　　　 　 　 　 ,　'´　　　　　//ｒ’ ´ /　A　＼　　 ヽ〉 　　　 ＼　_ｘ==////　　　ゝヽ￣ヽ　ｰ- '				//
//　　　　　 　 　 　 ,. '´　　　　　　 /Ｚ´》ii. / ∠､ ﾏ ∧` .トヾ　　　　.ｘ＜///////!　、_　　ヽ' ﾉ_,.〉				//
//　　 　 　 　 　 ／　 　 　 　 　 ./ 〔{ﾟ_}|',ﾄﾚｲιﾉ　V ,_ﾄ､ |ﾐ-ゝ ,ｘ＜//////////|ｘ｀＞､..ﾉ=┘						//
//　　 　 　 　 ／　　　　　　 　 /　　 ヾｔ λ ﾄ　　　､ιﾘλ!＞升////////////∧////|										//
//　　　　　 ／ 　 　 　 　　 　/　　　　　　 ｘ`　 -‐　　νィ´//////////////////X,//|									//
//　　　　／ 　 　 　　 　 　 /　　　　　　　　ﾉ` ､ 　彳'´　V///////ZZ≫=――===''''''´　 ＞ ､								//
//　 　 , '　　　　　　　　　 /　　　　　　 _i＜_ｒ'￣ｲ　 　 　 У´￣￣ ､　　　　　　　　　　　　　　　＞ ､				//
//　　/　　　　　　　　　　/ 　 　 　　 '",. |,ｲ_λ/ |　　　／　　　　　　 ＼　　　　　　　　　　　　　 　 　 ＞ ､			//
//　/　　　　　　　　　　 /　　　　 /,.ィ　　/　|　==Ｖ　 /　　　　　　　　　 ＼　　　　　　　　　　　　　　　　　 ` ､		//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int main_game(ALLEGRO_EVENT_QUEUE **event_queue, struct Loaded_Images **LoadedImages, ALLEGRO_EVENT *ev, ALLEGRO_FONT **fontPtr, ALLEGRO_SAMPLE_INSTANCE **songInstance, struct PlayerSound **pSounds)
{
	// Score stuff
	char scoreString[11] = { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0' };
	char currentScoreString[11];


	// Declarations
	double gameTimer = 0;
	int keyLock = 0;
	struct SnakeBody *travel = NULL;

	struct Apple *applePtr = malloc(sizeof(struct Apple));
	if (applePtr == NULL) {
		return -1;
	}

	int addNode = 0;
	int running = 1;
	int redraw = 0;
	int returnscore = 0;

	// PowerUp prototype
	struct PowerUp *powerUpPtr = malloc(sizeof(struct PowerUp));
	if (powerUpPtr == NULL) {
		return -1;
	}

	powerUpPtr->isSpawned = 0;
	powerUpPtr->powerType = 0;
	powerUpPtr->powerUpTimer = 0;

	// Faces prototype
	struct Face *Face = malloc(sizeof(struct Face));
	if (Face == NULL) {
		return -1;
	}

	Face->currentDefaultFace = 0;
	Face->currentFace = 0;
	Face->xPos = 750;
	Face->yPos = 322;
	Face->displayTimer = 0;

	// SnakeHead prototype
	struct SnakeHead *Head = malloc(sizeof(struct SnakeHead));
	if (Head == NULL) {
		return -1;
	}

	// Set default values
	Head->xPos = ((rand() % 20) * 15) + SPELPLAN_START_X;
	Head->yPos = ((rand() % 20) * 15) + SPELPLAN_START_Y;
	Head->direction = RIGHT;
	Head->length = 1;
	Head->nextPart = NULL;
	Head->isDead = 0;
	Head->currentScore = 0;
	Head->doubleScorePower = 1;			// Starts at one since it's used to multiply score
	Head->blindModePower = 0;
	Head->ghostModePower = 0;
	Head->invertedControlsPower = 0;
	Head->snakeSpeed = 10;
	Head->powerUpCounter = 0;

	// Start playing the song
	al_play_sample_instance(*songInstance);

	// Main loop
	while (running == 1) {
		al_wait_for_event(*event_queue, &*ev);

		if (ev->type == ALLEGRO_EVENT_TIMER && (Head->isDead != 1)) {

			// Check if apple should spawn
			if (applePtr->isAppleSpawned != 1) {
				spawn_apple(applePtr, Head);
			}

			// Set speed for ticks, do snake shit on tick
			gameTimer++;
			if (Face->displayTimer > 0) {
				Face->displayTimer--;
			}

			// Check if powerup should deactivate
			if (powerUpPtr->powerUpTimer > 0) {
				powerUpPtr->powerUpTimer--;

				if (powerUpPtr->powerUpTimer == 0) {
					// Reset everything
					Head->doubleScorePower = 1;
					Head->blindModePower = 0;
					Head->ghostModePower = 0;
					Head->invertedControlsPower = 0;
					Face->currentDefaultFace = powerUpPtr->currentDefaultFace;
				}

			}

			// Check if powerup should despawn
			if(powerUpPtr->spawnTimer > 0) {
				powerUpPtr->spawnTimer--;

				if (powerUpPtr->spawnTimer == 0) {
					powerUpPtr->isSpawned = 0;
					Head->powerUpCounter = 0;
				}
			}

			// Check for tick
			if (gameTimer >= Head->snakeSpeed) {

				// Testa addnode
				if (addNode == 1) {
					if (snake_grow(Head, travel, &addNode) == -1) {
						return -1;
					}

					// Check if a powerup should spawn
					if (Head->powerUpCounter == 5) {
						spawn_powerup(powerUpPtr, Head, applePtr);
					}
				}

				// Testa update snake
				if (Head->length > 1) {
					travel = Head->nextPart;
					update_snake(&travel, Head->xPos, Head->yPos);
				}

				// Move snake and check collisions
				move_snake(Head);
				check_collision(Head, applePtr, &addNode, Face, powerUpPtr, pSounds);

				// Update score, where were you when apple was kill?
				update_score(scoreString, currentScoreString, Head->currentScore);
				returnscore = Head->currentScore;


				// Reset
				keyLock = 0;
				gameTimer = 0;
			}

			redraw = 1;
		}

		// Check for keyboard input
		if (ev->type == ALLEGRO_EVENT_KEY_DOWN) {
			check_input(&*ev, &keyLock, Head, &addNode, &running);
		}

		// Check if the screen should be rendered and if the event queue is empty
		if (redraw == 1 && al_is_event_queue_empty(*event_queue)) {
			change_face(Face, Head, powerUpPtr);
			draw_screen(applePtr, &*LoadedImages, Head, travel, Face, &*fontPtr, scoreString, powerUpPtr);

			redraw = 0;
		}
	}

	//////////////////////////
	// Clean up snake stuff //
	//////////////////////////
	// Delete snake list
	travel = Head->nextPart;
	if (travel != NULL) {
		delete_list(&travel);
	}

	Head->nextPart = NULL;
	free(Head);

	// Free memory
	free(applePtr);

	al_stop_sample_instance(*songInstance);

	return returnscore;
}

///////////////////////////////////////////////
// Draw menu function, used in the main menu //
///////////////////////////////////////////////
void draw_menu(struct Loaded_Images **LoadedImages)
{
	// Clear screen
	al_clear_to_color(al_map_rgb(0, 0, 0));

	// Draw background
	al_draw_bitmap((*LoadedImages)->menuBackground, 0, 0, 0);

	// Render screen
	al_flip_display();
}

int main_menu(ALLEGRO_EVENT_QUEUE **event_queue, struct Loaded_Images **LoadedImages, ALLEGRO_EVENT *ev, ALLEGRO_SAMPLE_INSTANCE **menuSongInstance)
{
	int redraw = 0;
	int menu = 1;
	int running = 1;

	al_play_sample_instance(*menuSongInstance);

	while (menu == 1) {
		al_wait_for_event(*event_queue, &*ev);

		if (ev->type == ALLEGRO_EVENT_TIMER) {

			// Check if the screen should be rendered and if the event queue is empty
			if (redraw == 1 && al_is_event_queue_empty(*event_queue)) {
				draw_menu(&*LoadedImages);
				redraw = 0;
			}
			redraw = 1;
		}

		if (ev->type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) {

			// Start new game button
			if ((ev->mouse.x >= 343 && ev->mouse.x <= 677) && (ev->mouse.y >= 358 && ev->mouse.y <= 410)) {
				al_stop_sample_instance(*menuSongInstance);
				return 1;
			}

			// Highscore button
			if ((ev->mouse.x >= 355 && ev->mouse.x <= 665) && (ev->mouse.y >= 438 && ev->mouse.y <= 506)) {
				//al_stop_sample_instance(*menuSongInstance);
				return 2;
			}

			// Exit button
			if ((ev->mouse.x >= 453 && ev->mouse.x <= 566) && (ev->mouse.y >= 528 && ev->mouse.y <= 580)) {
				al_stop_sample_instance(*menuSongInstance);
				return 0;
			}
		}
	}

	return -1;
}
