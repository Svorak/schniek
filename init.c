//////////////////////////////////////////////////////////////////////////////////////////////////////
//  Namn:   Jimmy M�kkeli & Carl Larsson                                                            //
//  ID:     jmi14002 & cln14008                                                                     //
//                                                                                                  //
//  Beskrivning                                                                                     //
//  Den h�r funktionen har hand om all Allegrospecifik kod som hanterar t.ex. installation av		//
//  funktioner som display, keyboard/mus input, ljudhantering osv.									//
//	Det finns �ven funktioner f�r att ladda in bilder, ljud och typsnitt i spelet.					//
//  Det finns felhantering vid varje steg.															//
//////////////////////////////////////////////////////////////////////////////////////////////////////
#include "structs.h"

#define SCREEN_WIDTH 1024
#define SCREEN_HEIGHT 768
#define FPS 60

/////////////////////////////////////////////////////////////////////////////////////////////////////
// Load allegro stuff, return -1 and display an error message if something fails to be initialized //
/////////////////////////////////////////////////////////////////////////////////////////////////////
int init_stuff(ALLEGRO_DISPLAY **display, ALLEGRO_EVENT_QUEUE **event_queue, ALLEGRO_TIMER **timer)
{
	if (!al_init()) {
		al_show_native_message_box(*display, "Error", "Error", "Failed to initialize allegro!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return -1;
	}

	if (!al_init_image_addon()) {
		al_show_native_message_box(*display, "Error", "Error", "Failed to initialize al_init_image_addon!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return -1;
	}

	*display = al_create_display(SCREEN_WIDTH, SCREEN_HEIGHT);

	if (!*display) {
		al_show_native_message_box(*display, "Error", "Error", "Failed to initialize display!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return -1;
	}

	*timer = al_create_timer(1.0 / FPS);
	if (!timer) {
		al_show_native_message_box(*display, "Error", "Error", "Failed to initialize timer!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return -1;
	}

	*event_queue = al_create_event_queue();
	if (!*event_queue) {
		al_show_native_message_box(*display, "Error", "Error", "Failed to initialize event!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return -1;
	}

	if (!al_install_keyboard()) {
		al_show_native_message_box(*display, "Error", "Error", "Failed to initialize keyboard!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return -1;
	}

	if (!al_install_audio()){
		al_show_native_message_box(*display, "Error", "Error", "Failed to initialize audio!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return -1;
	}

	if (!al_init_acodec_addon()) {
		al_show_native_message_box(*display, "Error", "Error", "Failed to initialize acodec!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return -1;
	}

	if (!al_reserve_samples(2)) {
		al_show_native_message_box(*display, "Error", "Error", "Failed to initialize reserve samples!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return -1;
	}

	if (!al_install_mouse()) {
		al_show_native_message_box(*display, "Error", "Error", "Failed to initialize mouse!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return -1;
	}

	al_init_font_addon(); // initialize the font addon
	al_init_ttf_addon();  // initialize the ttf (True Type Font) addon

	// Register event sources, used to check stuff like timer events, or keyyboard/mouse input
	al_register_event_source(*event_queue, al_get_display_event_source(*display));
	al_register_event_source(*event_queue, al_get_timer_event_source(*timer));
	al_register_event_source(*event_queue, al_get_keyboard_event_source());
	al_register_event_source(*event_queue, al_get_mouse_event_source());

	return 1;
}

//////////////////////////////////////////////////
// Loads all the .png files, has error checking //
//////////////////////////////////////////////////
int load_images(struct Loaded_Images **LoadedImages, ALLEGRO_DISPLAY **display)
{
	int i = 0;
	// Load bitmaps
	(*LoadedImages)->snakePart = al_load_bitmap("img/snake_part.png");
	(*LoadedImages)->apple = al_load_bitmap("img/apple.png");
	(*LoadedImages)->appleBonusPoint = al_load_bitmap("img/applebonus.png");
	(*LoadedImages)->background = al_load_bitmap("img/BG/background.png");
	(*LoadedImages)->menuBackground = al_load_bitmap("img/BG/menu_bg.png");
	(*LoadedImages)->highscoreBackground = al_load_bitmap("img/BG/highscore_bg.png");

	// Check if there was an error loading images
	if (!(*LoadedImages)->snakePart || !(*LoadedImages)->apple || !(*LoadedImages)->background || !(*LoadedImages)->appleBonusPoint || !(*LoadedImages)->menuBackground || !(*LoadedImages)->highscoreBackground){
		return -1;
	}

	// Load faces
	(*LoadedImages)->Faces[0] = al_load_bitmap("img/Faces/Face0.png");
	(*LoadedImages)->Faces[1] = al_load_bitmap("img/Faces/Face1.png");
	(*LoadedImages)->Faces[2] = al_load_bitmap("img/Faces/Face2.png");
	(*LoadedImages)->Faces[3] = al_load_bitmap("img/Faces/Face3.png");
	(*LoadedImages)->Faces[4] = al_load_bitmap("img/Faces/Face4.png");
	(*LoadedImages)->Faces[5] = al_load_bitmap("img/Faces/Face5.png");
	(*LoadedImages)->Faces[6] = al_load_bitmap("img/Faces/Face6.png");
	(*LoadedImages)->Faces[7] = al_load_bitmap("img/Faces/Face7.png");
	(*LoadedImages)->Faces[8] = al_load_bitmap("img/Faces/Face8.png");
	(*LoadedImages)->Faces[9] = al_load_bitmap("img/Faces/Face9.png");

	// Check if all faces loaded correctly
	for (i = 0; i < 10; i++) {
		if (!(*LoadedImages)->Faces[i]) {
			return -1;
		}
	}

	// Load powerups
	(*LoadedImages)->PowerUps[0] = al_load_bitmap("img/PowerUp_DoubleScore.png");
	(*LoadedImages)->PowerUps[1] = al_load_bitmap("img/PowerUp_Blind.png");
	(*LoadedImages)->PowerUps[2] = al_load_bitmap("img/PowerUp_SlowDown.png");
	(*LoadedImages)->PowerUps[3] = al_load_bitmap("img/PowerUp_Ghost.png");
	(*LoadedImages)->PowerUps[4] = al_load_bitmap("img/PowerUp_InvertControls.png");

	// Check if all power ups loaded correctly
	for (i = 0; i < 5; i++) {
		if (!(*LoadedImages)->PowerUps[i]) {
			return -1;
		}
	}

	return 1;
}

///////////////////////////////////////////////////////////
// Loads all the .ogg and .wav files, has error checking //
///////////////////////////////////////////////////////////
int load_sample(ALLEGRO_SAMPLE **mainSample, ALLEGRO_SAMPLE_INSTANCE **mainSongInstance, ALLEGRO_SAMPLE **menuSample, ALLEGRO_SAMPLE_INSTANCE **menuSongInstance, struct PlayerSound **pSoundsPtr)
{
	// Load music
	*mainSample = al_load_sample("snd/track01.ogg");
	*menuSample = al_load_sample("snd/track02.ogg");

	if (!mainSample || !menuSample) {
		return -1;
	}

	// Set instance mode to allow longer audio samples and looping of sound
	*mainSongInstance = al_create_sample_instance(*mainSample);
	al_set_sample_instance_playmode(*mainSongInstance, ALLEGRO_PLAYMODE_LOOP);
	al_attach_sample_instance_to_mixer(*mainSongInstance, al_get_default_mixer());

	*menuSongInstance = al_create_sample_instance(*menuSample);
	al_set_sample_instance_playmode(*menuSongInstance, ALLEGRO_PLAYMODE_LOOP);
	al_attach_sample_instance_to_mixer(*menuSongInstance, al_get_default_mixer());

	// Check for errors
	if (!mainSongInstance || !menuSongInstance) {
		return -1;
	}

	// Load audio samples
	(*pSoundsPtr)->applePickup = al_load_sample("snd/regapple.wav");
	(*pSoundsPtr)->blindnessPickup = al_load_sample("snd/blindness.wav");
	(*pSoundsPtr)->bonusApplePickup = al_load_sample("snd/bonuspointapple.wav");
	(*pSoundsPtr)->death = al_load_sample("snd/death.wav");
	(*pSoundsPtr)->powerUpPickup = al_load_sample("snd/powerup.wav");

	// Check for errors
	if (!(*pSoundsPtr)->applePickup || !(*pSoundsPtr)->blindnessPickup || !(*pSoundsPtr)->bonusApplePickup || !(*pSoundsPtr)->death || !(*pSoundsPtr)->powerUpPickup) {
		return -1;
	}

	return 1;
}
////////////////////
// Loads the font //
////////////////////
int load_font(ALLEGRO_FONT **fontPtr)
{
	 *fontPtr = al_load_ttf_font("MODES.TTF", 36, 0);

	if (!*fontPtr){
		return -1;
	}

	return 1;
}
