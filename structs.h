//////////////////////////////////////////////////////////////////////////////////////////////////////
//  Namn:   Jimmy M�kkeli & Carl Larsson                                                            //
//  ID:     jmi14002 & cln14008                                                                     //
//                                                                                                  //
//  Beskrivning                                                                                     //
//  Den h�r filen har hand om alla deklarationer f�r structs och enums. H�r inkluderas �ven			//
//  Allegrospecifika och standard headerfiler.Den inneh�ller �ven #defines f�r olika v�rden.		//
//////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef STRUCTS_H
#define STRUCTS_H

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_native_dialog.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

#define SPELPLAN_START_X 60
#define SPELPLAN_START_Y 105
#define CELL_SIZE 15

struct PlayerSound {
	ALLEGRO_SAMPLE *applePickup;
	ALLEGRO_SAMPLE *bonusApplePickup;
	ALLEGRO_SAMPLE *blindnessPickup;
	ALLEGRO_SAMPLE *powerUpPickup;
	ALLEGRO_SAMPLE *death;
};

struct PowerUp {
	int xPos;
	int yPos;

	int isSpawned;
	int spawnTimer;		// Used to despawn powerup
	int powerUpTimer;	// Used to change face based on powerup
	int powerType;		// 0 = double score, 1 = blind, 2 = slow down, 3 = ghost, 4 = inverted controls

	int currentDefaultFace;
};

struct SnakeHead {
	int xPos;
	int yPos;
	struct SnakeBody *nextPart;

	int direction;
	int length;

	int isDead;
	int currentScore;

	double snakeSpeed;

	int doubleScorePower;
	int blindModePower;
	int ghostModePower;
	int invertedControlsPower;
	int powerUpCounter;		// Used to spawn powerups
};

struct SnakeBody {
	int xPos;
	int yPos;
	struct SnakeBody *nextPart;
};

struct Apple {
	int xPos;
	int yPos;

	int appleType;
	int isAppleSpawned;
};

struct Loaded_Images {
	ALLEGRO_BITMAP *snakePart;
	ALLEGRO_BITMAP *apple;
	ALLEGRO_BITMAP *appleBonusPoint;
	ALLEGRO_BITMAP *background;
	ALLEGRO_BITMAP *menuBackground;
	ALLEGRO_BITMAP *highscoreBackground;

	ALLEGRO_BITMAP *Faces[10];
	ALLEGRO_BITMAP *PowerUps[5];
};

struct Face {
	int xPos;
	int yPos;

	int currentFace;
	int currentDefaultFace;

	int displayTimer;
};

struct HighScoreList {
	char name[11];
	char score[10];
	struct HighScoreList *next;
};

enum KEYS {
	UP, DOWN, LEFT, RIGHT
};

#endif
