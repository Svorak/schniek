//////////////////////////////////////////////////////////////////////////////////////////////////////
//  Namn:   Jimmy M�kkeli & Carl Larsson                                                            //
//  ID:     jmi14002 & cln14008                                                                     //
//                                                                                                  //
//  Beskrivning                                                                                     //
//  Den h�r filen har hand om alla funktioner som �r specifika till sj�lva spelet. Hantering av     //
//  kollision, f�rflyttning, hur ormen v�xer.														//
//  Den hanterar �ven uppdatering av spelarens po�ng och frig�ranet av minne i den l�nkade listan   //
//  och hur nya �pplen och power up skapas															//
//  p� spelplanen.																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////
#include "structs.h"
#include "init.h"
#include "snake.h"

//////////////////////////////////////////////////////////
// Handles the creation of new nodes in the linked list //
//////////////////////////////////////////////////////////
int add_node(struct SnakeBody **travel, int xPos, int yPos)
{
	// Create a new node, add values to X & Y and set the last pointer to NULL
	(*travel)->nextPart = malloc(sizeof(struct SnakeBody));
	if ((*travel)->nextPart == NULL) {
		return -1;
	}

	(*travel)->nextPart->xPos = xPos;
	(*travel)->nextPart->yPos = yPos;

	(*travel) = (*travel)->nextPart;
	(*travel)->nextPart = NULL;

	return 0;
}

////////////////////////////////////////////////////////////
// Updates the X and Y position of each node in the snake //
////////////////////////////////////////////////////////////
void update_snake(struct SnakeBody **travel, int x, int y)
{
	int tempX = 0;
	int tempY = 0;

	// Change the first node
	tempX = (*travel)->xPos;
	tempY = (*travel)->yPos;
	(*travel)->xPos = x;
	(*travel)->yPos = y;

	(*travel) = (*travel)->nextPart;

	// Push back new values
	while ((*travel) != NULL) {
		// Swap tempX
		tempX ^= (*travel)->xPos;
		(*travel)->xPos ^= tempX;
		tempX ^= (*travel)->xPos;

		// Swap tempY
		tempY ^= (*travel)->yPos;
		(*travel)->yPos ^= tempY;
		tempY ^= (*travel)->yPos;

		(*travel) = (*travel)->nextPart;
	}
}

/****************************************
* Free memory for each node in the list *
****************************************/
void delete_list(struct SnakeBody **travel)
{
	struct SnakeBody *temp;

	// Scan the list, set temp as current node, travel to the next and delete temp
	while ((*travel) != NULL) {
		temp = (*travel);
		(*travel) = (*travel)->nextPart;
		temp->nextPart = NULL;
		free(temp);
	}
	temp = NULL;
}

/***********************
* Handles snake growth *
***********************/
int snake_grow(struct SnakeHead *HeadPtr, struct SnakeBody *travelPtr, int *addNodePtr)
{
	/*****************************************
	* If it's not the first node being added *
	*****************************************/
	// Set travel to nextPart pointer
	travelPtr = HeadPtr->nextPart;

	// If the length is > 1, scan the list to find the last node
	if (HeadPtr->length > 1) {
		while (travelPtr->nextPart != NULL) {
			travelPtr = travelPtr->nextPart;
		}
		// Add node
		if (add_node(&travelPtr, 1, 1) == -1) {
			return -1;
		}
	}

	/*************************************
	* If it's the first node being added *
	*************************************/
	// If lenght == 1, create the first SnakeBody node
	else if (HeadPtr->length == 1) {
		HeadPtr->nextPart = malloc(sizeof(struct SnakeBody));
		if (HeadPtr->nextPart == NULL) {
			return -1;
		}

		HeadPtr->nextPart->xPos = HeadPtr->xPos;
		HeadPtr->nextPart->yPos = HeadPtr->yPos;

		travelPtr = HeadPtr->nextPart;
		travelPtr->nextPart = NULL;
	}


	*addNodePtr = 0;
	HeadPtr->length++;

	/***************************************
	 * Check if game speed should increase *
	 **************************************/
	// If the length has changed by three nodes, increase the speed
	if ((HeadPtr->length) % 3 == 0 && HeadPtr->snakeSpeed > 0.1) {
		HeadPtr->snakeSpeed /= 1.2;
	}

	return 0;
}
/******************************
* Handles collision detection *
******************************/
void check_collision(struct SnakeHead *HeadPtr, struct Apple *applePtr, int *addNodePtr, struct Face *FacePtr, struct PowerUp *powerUpPtr, struct PlayerSound **pSounds)
{
    struct SnakeBody *temp = NULL;

	// Check for apples
	if ((HeadPtr->xPos == applePtr->xPos) && (HeadPtr->yPos == applePtr->yPos)) {
		*addNodePtr = 1;
		applePtr->isAppleSpawned = 0;

		// Set a new current face
		FacePtr->currentFace = 1;
		FacePtr->displayTimer = 60;

		// Add score
		HeadPtr->currentScore += (10 * (applePtr->appleType) * (HeadPtr->doubleScorePower) );
		HeadPtr->powerUpCounter++;

		// Play sound
		if (applePtr->appleType == 1) {
			al_play_sample((*pSounds)->applePickup, 2.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
		}
		else {
			al_play_sample((*pSounds)->bonusApplePickup, 2.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
		}
	}

	// Check for powerup
	if (powerUpPtr->isSpawned == 1 && ((HeadPtr->xPos == powerUpPtr->xPos) && (HeadPtr->yPos == powerUpPtr->yPos))) {
		// Reset stuff
		powerUpPtr->isSpawned = 0;
		HeadPtr->powerUpCounter = 0;

		// Save current face
		powerUpPtr->currentDefaultFace = FacePtr->currentDefaultFace;

		// Check powerup type - 0 = double score, 1 = blind, 2 = slow down, 3 = ghost, 4 = inverted controls
		if (powerUpPtr->powerType == 0) {
			HeadPtr->doubleScorePower = 2;
			FacePtr->currentDefaultFace = 2;
			powerUpPtr->powerUpTimer = 300;		// 5 seconds

			HeadPtr->currentScore += 50;
		}

		else if (powerUpPtr->powerType == 1) {
			HeadPtr->blindModePower = 1;
			FacePtr->currentDefaultFace = 9;
			powerUpPtr->powerUpTimer = 120;		// 2 seconds

			HeadPtr->currentScore += 300;
		}

		else if (powerUpPtr->powerType == 2) {
			if (HeadPtr->snakeSpeed < 3) {
				HeadPtr->snakeSpeed = 3;
			}
			HeadPtr->currentScore += 500;
		}

		else if (powerUpPtr->powerType == 3) {
			FacePtr->currentDefaultFace = 8;
			powerUpPtr->powerUpTimer = 300;		// 5 seconds
			HeadPtr->ghostModePower = 1;

			HeadPtr->currentScore += 700;
		}

		else if (powerUpPtr->powerType == 4) {
			FacePtr->currentDefaultFace = 4;
			powerUpPtr->powerUpTimer = 180;		// 3 seconds
			HeadPtr->invertedControlsPower = 1;

			HeadPtr->currentScore += 1000;
		}

		// Play sound
		if (powerUpPtr->powerType == 1) {
			al_play_sample((*pSounds)->blindnessPickup, 2.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
		}

		else {
			al_play_sample((*pSounds)->powerUpPickup, 2.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
		}
	}

    // Check self-collision
	if (HeadPtr->length > 1 && HeadPtr->ghostModePower == 0) {
        temp = HeadPtr->nextPart;

        // Scan the linke list and check for collisions
        while (temp != NULL) {
            if (HeadPtr->xPos == temp->xPos && HeadPtr->yPos == temp->yPos) {
                    HeadPtr->isDead = 1;
            }

            temp = temp->nextPart;
        }
	}

	//////////////////////////////////////////////////////////////////////////
	// Check for border collision, if ghost mode is active, the snake warps //
	//////////////////////////////////////////////////////////////////////////
	// Check left wall
	if (HeadPtr->xPos < 60) {
		if (HeadPtr->ghostModePower == 1) {
			HeadPtr->xPos = 645;
		}
		else {
			HeadPtr->isDead = 1;
		}
	}

	// Check top wall
	if (HeadPtr->yPos < 105) {
		if (HeadPtr->ghostModePower == 1) {
			HeadPtr->yPos = 690;
		}
		else {
			HeadPtr->isDead = 1;
		}
	}

	// Check right wall
	if (HeadPtr->xPos > 645) {
		if (HeadPtr->ghostModePower == 1) {
			HeadPtr->xPos = 60;
		}
		else {
			HeadPtr->isDead = 1;
		}
	}

	// Check bottom wall
	if (HeadPtr->yPos > 690) {
		if (HeadPtr->ghostModePower == 1) {
			HeadPtr->yPos = 105;
		}
		else {
			HeadPtr->isDead = 1;
		}
	}

	// Play death sound
	if (HeadPtr->isDead == 1) {
		al_play_sample((*pSounds)->death, 2.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
	}
}
/*************************
* Handles snake movement *
*************************/
int move_snake(struct SnakeHead *HeadPtr)
{
	switch (HeadPtr->direction){
        case UP:
            HeadPtr->yPos -= CELL_SIZE;
            break;

        case DOWN:
            HeadPtr->yPos += CELL_SIZE;
            break;

        case LEFT:
            HeadPtr->xPos -= CELL_SIZE;
            break;

        case RIGHT:
            HeadPtr->xPos += CELL_SIZE;
            break;
	}

	return 1;
}

/********************************************
* Handles string conversion from int, score *
*********************************************/
void update_score(char scoreString[11], char currentScoreString[11], int currScore)
{
	unsigned int i = 0;

	//itoa(currScore, currentScoreString, 10);
    sprintf(currentScoreString, "%d", currScore);

	for (i = 0; i < strlen(currentScoreString); i++) {
		scoreString[(strlen(scoreString) - i) - 1] = currentScoreString[(strlen(currentScoreString) - i) - 1];
	}
	
}

/////////////////////////////////////////////////////////////////////////////////////////////////
// Handles powerup spawning, performs checks to prevent the powerup from spawning inside stuff //
/////////////////////////////////////////////////////////////////////////////////////////////////
void spawn_powerup(struct PowerUp *powerUpPtr, struct SnakeHead *headPtr, struct Apple *applePtr)
{
	int spawnOk = 0;
	int randPowerType = 0;
	struct SnakeBody *temp = NULL;

	// Loop to check apple spawn pos
	while (spawnOk == 0) {
		spawnOk = 1;

		// Randomize pos
		powerUpPtr->xPos = ((rand() % 40) * CELL_SIZE) + SPELPLAN_START_X;
		powerUpPtr->yPos = ((rand() % 40) * CELL_SIZE) + SPELPLAN_START_Y;

		// Check if powerup spawns inside the head
		if (!(powerUpPtr->xPos == headPtr->xPos && powerUpPtr->yPos == headPtr->yPos)) {

			temp = headPtr->nextPart;

			// Check if powerup spawns inside the body
			while (temp != NULL) {
				if (powerUpPtr->xPos == temp->xPos && powerUpPtr->yPos == temp->yPos) {
					spawnOk = 0;
					break;
				}
				temp = temp->nextPart;
			}

			// Check if powerup spawns inside apple
			if (powerUpPtr->xPos == applePtr->xPos && powerUpPtr->yPos == applePtr->yPos) {
				spawnOk = 0;
			}

		}
	}

	// Randomize powerup types, 0 = double score, 1 = blind, 2 = slow down, 3 = ghost, 4 = inverted controls
	randPowerType = rand() % 100;

	if (randPowerType <= 40) {
		powerUpPtr->powerType = 0;
	}

	else if (randPowerType > 40 && randPowerType <= 60) {
		powerUpPtr->powerType = 1;
	}

	else if (randPowerType > 60 && randPowerType <= 80) {
		powerUpPtr->powerType = 3;
	}

	else if (randPowerType > 80 && randPowerType <= 90) {
		powerUpPtr->powerType = 4;
	}

	else if (randPowerType > 90) {
		powerUpPtr->powerType = 2;
	}

	powerUpPtr->isSpawned = 1;
	powerUpPtr->spawnTimer = 300;
}

////////////////////////////////////////////////////////////////////////////////////////
// Handles apple spawning, performs checks to prevent apple spawning inside the snake //
////////////////////////////////////////////////////////////////////////////////////////
void spawn_apple(struct Apple *applePtr, struct SnakeHead *headPtr)
{
	int spawnOk = 0;
	int randAppleType = 0;
	struct SnakeBody *temp = NULL;

	// Loop to check apple spawn pos
	while (spawnOk == 0) {
		spawnOk = 1;

		// Randomize pos
		applePtr->xPos = ((rand() % 40) * CELL_SIZE) + SPELPLAN_START_X;
		applePtr->yPos = ((rand() % 40) * CELL_SIZE) + SPELPLAN_START_Y;

		// Check if apple spawns inside the head
		if (!(applePtr->xPos == headPtr->xPos && applePtr->yPos == headPtr->yPos)) {

			temp = headPtr->nextPart;

			// Check i apple spawns inside the body
			while (temp != NULL) {
				if (applePtr->xPos == temp->xPos && applePtr->yPos == temp->yPos) {
					spawnOk = 0;
					break;
				}
				temp = temp->nextPart;
			}
		}
	}

	// Randomize apple types, applytype = multiplier or base score per apple
	randAppleType = rand() % 100;

	if (randAppleType <= 70) {
		applePtr->appleType = 1;
	}

	else if (randAppleType > 70 && randAppleType <= 90) {
		applePtr->appleType = 2;
	}

	else if (randAppleType > 90) {
		applePtr->appleType = 3;
	}

	applePtr->isAppleSpawned = 1;
}

/////////////////////////////////////////////////////////////////////////////
// This function changes face depending on current speed or if you're dead //
/////////////////////////////////////////////////////////////////////////////
void change_face(struct Face *Face, struct SnakeHead *HeadPtr, struct PowerUp *powerUpPtr)
{
	if (HeadPtr->snakeSpeed >= 10 && powerUpPtr->powerUpTimer == 0)
		Face->currentDefaultFace = 0;

	else if ((HeadPtr->snakeSpeed <= 5 && HeadPtr->snakeSpeed > 2) && powerUpPtr->powerUpTimer == 0)
		Face->currentDefaultFace = 3;

	else if (HeadPtr->snakeSpeed < 1 && powerUpPtr->powerUpTimer == 0)
		Face->currentDefaultFace = 7;

	if (HeadPtr->isDead == 1) {
		Face->currentDefaultFace = 6;
	}
}
