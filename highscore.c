//////////////////////////////////////////////////////////////////////////////////////////////////////
//  Namn:   Jimmy M�kkeli & Carl Larsson                                                            //
//  ID:     jmi14002 & cln14008                                                                     //
//                                                                                                  //
//  Beskrivning                                                                                     //
//  Den h�r filen har hand om all hantering av High Score listan, hur programmet l�ser av textfilen //
//	och placerar in alla spelare och deras po�ng i en l�nkad lista, samt hantering av att l�gga till//
//	nya High Scores.Det finns �ven funktioner f�r hur listan ska sparas och hur minnet frig�rs n�r  //
//	allt �r f�rdigt.																				//
//////////////////////////////////////////////////////////////////////////////////////////////////////
#include "structs.h"
#include "snake.h"
#include "highscore.h"

/////////////////////////////
// Main highscore function //
/////////////////////////////
int highscore_menu(ALLEGRO_EVENT_QUEUE **event_queue, struct Loaded_Images **LoadedImages, ALLEGRO_EVENT *ev, ALLEGRO_SAMPLE_INSTANCE **menuSongInstance, ALLEGRO_FONT **fontPtr, int tempScore)
{
	int running = 1;
	int redraw = 0;
	int temp;
	int inputAllowed = 1;
	int currCharInString = 0;
	char finished[11];
	char file[20] = "highscore.txt";
	char scoreString[11] = { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0' };
	char currentScoreString[11];
	char name[11];
	name[0] = '\0';
	struct HighScoreList *root = NULL;
	struct HighScoreList *current = NULL;
	struct HighScoreList *previous = NULL;
	struct HighScoreList *travel = NULL;

	FILE *textFile = NULL;

	al_play_sample_instance(*menuSongInstance);
	// Try to open file
	if (openFile(&textFile, file) == -1) {
		return -2;
	}

	// Create the first node in the list
	root = malloc(sizeof(struct HighScoreList));
	root->next = NULL;

	// Read list
	current = root;
	if (fillList(&textFile, &current) == -1)
	{
		return -1;
	}
	//Turns int into a string in the formate needed
	update_score(scoreString, currentScoreString, tempScore);

	// Print list
	current = root;
	temp = searchList(&current, &previous, scoreString);
	if (temp == 0) {
		inputAllowed = 0;
	}


	while (running == 1) {
		al_wait_for_event(*event_queue, *&ev);

		if (temp > 0) {
			if (inputAllowed == 1) {

				// Read keyboard input
				if (ev->type == ALLEGRO_EVENT_KEY_CHAR) {

					// Check for backspace
					if (ev->keyboard.keycode == ALLEGRO_KEY_BACKSPACE) {
						if (currCharInString > 0) {
							name[currCharInString - 1] = '\0';
							currCharInString--;
						}
					}

					// Check for enter
					else if (ev->keyboard.keycode == ALLEGRO_KEY_ENTER) {
						inputAllowed = 2;
						strcpy(finished, name);

					}

					// Check for character
					else {
						if (currCharInString < 10) {
							name[currCharInString] = ev->keyboard.unichar;
							currCharInString++;
							name[currCharInString] = '\0';
						}
					}
				}
			}
			//Places a new score in the list then saves the list
			if (temp == 1 && inputAllowed == 2) {

				placeNewScore(&current, &previous, scoreString, finished);
				current = root;
				saveListToFile(&textFile, file, &current);
				fclose(textFile);
				temp = 0;
			}

			//Places a new score at the top of the list then saves the list
			else if (temp == 2 && inputAllowed == 2) {
				root = placeNewHighestScore(&root, scoreString, finished);
				if (root == NULL)
				{
					return -1;
				}
				current = root;
				saveListToFile(&textFile, file, &current);
				fclose(textFile);
				temp = 0;
			}
		}
		//Draws the hichscore screen
		if (ev->type == ALLEGRO_EVENT_TIMER) {
			if (redraw == 1 && al_is_event_queue_empty(*event_queue)) {
				travel = root;
				outPutList(&travel, *&LoadedImages, *&fontPtr, inputAllowed, name);
				redraw = 0;
			}
			redraw = 1;

		}
		//Allows user to click return to main menu while also freeing memory
		if (ev->type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) {
			if ((ev->mouse.x >= 181 && ev->mouse.x <= 857) && (ev->mouse.y >= 694 && ev->mouse.y <= 772)) {
				FreeMemory(&root);
				return 0;
			}
		}
	}

	return 0;
}
//////////////////////////////////
// Opens highscore file for use //
//////////////////////////////////
int openFile(FILE **textFilePtr, char wordPtr[13])
{
	(*textFilePtr) = fopen(wordPtr, "r");

	//Checks so the file can be opend
	if ((*textFilePtr) == NULL) {
		return -1;
	}
	return 1;
}
//////////////////////////
// Fills list from file //
//////////////////////////
int fillList(FILE **textFilePtr, struct HighScoreList **currentPtr)
{
	char line[20];
	int i = 0;

	while (fgets(line, 20, *textFilePtr) != NULL)  {
		//Fills list with the proper format
		if (i < 19)
		{
			line[strlen(line) - 1] = '\0';
		}
		//Checks if its name or score to save
		if ((i % 2) == 0 || i == 0) {
			strcpy((*currentPtr)->name, line);

		}
		//Since the score is last it creates a new struct so the next item can be saved
		else
		{
			strcpy((*currentPtr)->score, line);

			(*currentPtr)->next = malloc(sizeof(struct HighScoreList));

			if (*currentPtr == NULL) {
				return -1;
			}

			(*currentPtr) = (*currentPtr)->next;
			(*currentPtr)->next = NULL;
		}

		i++;

	}
	return 0;
}
//////////////////////////
// Draws list to screen //
//////////////////////////
void outPutList(struct HighScoreList **currentPtr, struct Loaded_Images **LoadedImages, ALLEGRO_FONT **fontPtr, int inputAllowed, char name[11])
{
	int i = 0;
	int j = 157;
	int redraw = 1;
		//Clears screen
		al_clear_to_color(al_map_rgb(0, 0, 0));
		//Loads background picture
		al_draw_bitmap((*LoadedImages)->highscoreBackground, 0, 0, 0);

		//Outputs the list to he screen increasing the Y point everytime to make it a list
		while ( i < 10)
		{
			i++;
			al_draw_text(*fontPtr, al_map_rgb(255, 255, 255), 63, j, ALLEGRO_ALIGN_LEFT, (*currentPtr)->name);
			al_draw_text(*fontPtr, al_map_rgb(255, 255, 255), 710, j, ALLEGRO_ALIGN_RIGHT, (*currentPtr)->score);
			(*currentPtr) = (*currentPtr)->next;
			j += 49;
		}
		//If the user has a new highscore it activates text input
		if (inputAllowed == 1)
		{
			al_draw_text(*fontPtr, al_map_rgb(255, 255, 255), 63, 642, ALLEGRO_ALIGN_LEFT, name);
		}
		//Sends everything to be drawn to the screen.
		al_flip_display();
		redraw = 0;

	redraw = 1;
}
//////////////////
// Frees memory //
//////////////////
void FreeMemory(struct HighScoreList **currentPtr)
{
	struct HighScoreList *temp = NULL;
	int i = 1;

	while ((*currentPtr) != NULL) {
		temp = *currentPtr;
		(*currentPtr) = (*currentPtr)->next;
		free(temp);

	}
}
//////////////////////////////////////////////
// Searches list to check for new highscore //
//////////////////////////////////////////////
int searchList(struct HighScoreList **currentPtr, struct HighScoreList **previous, char testScore[11])
{
	int i = 0;
	int LS;
	while (i < 10)
	{
		i++;
		//Checks value from strcmp
		LS = strcmp((*currentPtr)->score, testScore);
		//if the new score is larger
		if (LS < 0)
		{
			//If the new score is larger than the first item
			if (i == 1)
			{
				return 2;
			}
			return 1;
		}
		//Continues on through the list
		else
		{
			(*previous) = (*currentPtr);
			(*currentPtr) = (*currentPtr)->next;
		}
	}
	return 0;
}
////////////////////////////////////////////////////////////////
// Places a new score in the list that isnt the highest score //
////////////////////////////////////////////////////////////////
void placeNewScore(struct HighScoreList **travel, struct HighScoreList **previous, char testScore[11], char testName[11])
{
	//Creates a struct inbetween two structs
	(*previous)->next = malloc(sizeof(struct HighScoreList));
	(*previous) = (*previous)->next;
	(*previous)->next = (*travel);
	strcpy((*previous)->score, testScore);
	strcpy((*previous)->name, testName);


}
///////////////////////////////////////////////
// Places a new score at the top of the list //
///////////////////////////////////////////////
struct HighScoreList *placeNewHighestScore(struct HighScoreList **currentPtr, char testScore[11], char testName[11])
{
	//Creats a new root struct
	struct HighScoreList *temp2 = NULL;

	temp2 = malloc(sizeof(struct HighScoreList));
	if (temp2 == NULL) {
		return NULL;
	}
	strcpy(temp2->score, testScore);
	strcpy(temp2->name, testName);
	temp2->next = (*currentPtr);

	return temp2;
}
////////////////////////////
// Saves the list to file //
////////////////////////////
void saveListToFile(FILE **textFilePtr, char wordPtr[13], struct HighScoreList **currentPtr)
{
	int i = 0;
	char test[3] = "\n";
	//Clears file and allows writing
	(*textFilePtr) = freopen(wordPtr, "w", stdout);

	for (i = 0; i < 10; i++) {
		fprintf(*textFilePtr, (*currentPtr)->name);
		fprintf(*textFilePtr, test);
		fprintf(*textFilePtr, (*currentPtr)->score);

		if (i < 9) {
			fprintf(*textFilePtr, test);
		}

		(*currentPtr) = (*currentPtr)->next;
	}
}
