//////////////////////////////////////////////////////////////////////////////////////////////////////
//  Namn:   Jimmy M�kkeli & Carl Larsson                                                            //
//  ID:     jmi14002 & cln14008                                                                     //
//                                                                                                  //
//  Beskrivning                                                                                     //
//  Den h�r funktionen har hand om all Allegrospecifik kod som hanterar t.ex. installation av		//
//  funktioner som display, keyboard/mus input, ljudhantering osv.									//
//	Det finns �ven funktioner f�r att ladda in bilder, ljud och typsnitt i spelet.					//
//  Det finns felhantering vid varje steg.															//
//////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef INIT_H
#define INIT_H

#include "structs.h"

int init_stuff(ALLEGRO_DISPLAY **display, ALLEGRO_EVENT_QUEUE **event_queue, ALLEGRO_TIMER **timer);
int load_sample(ALLEGRO_SAMPLE **mainSample, ALLEGRO_SAMPLE_INSTANCE **mainSongInstance, ALLEGRO_SAMPLE **menuSample, ALLEGRO_SAMPLE_INSTANCE **menuSongInstance, struct PlayerSound **pSoundsPtr);
int load_images(struct Loaded_Images **LoadedImages, ALLEGRO_DISPLAY **display);
int load_font(ALLEGRO_FONT **fontPtr);

#endif